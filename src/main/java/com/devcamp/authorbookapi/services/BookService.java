package com.devcamp.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.models.Book;

@Service
public class BookService extends AuthorService{
    Book book1 = new Book("How to be good", getAuthorBook1(), 120000, 5);
    Book book2 = new Book("Beauty and the beast", getAuthorBook2(), 80000, 2);
    Book book3 = new Book("Calligraphy and life", getAuthorBook3(), 90000, 3);
    Book book4 = new Book("Your advisor", getAuthorBook4(), 110000, 7);
    Book book5 = new Book("Let it snows", getAuthorBook5(), 50000, 1);
    Book book6 = new Book("Foods and thoughts", getAuthorBook6(), 190000, 6);
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        bookList.add(book4);
        bookList.add(book5);
        bookList.add(book6);
        return bookList;
    }
    public Book getBookById(int index){
        Book book = null;
        if (index >= 0 && index < getAllBooks().size()){
            book = getAllBooks().get(index);
        }
        return book;
    }
}
