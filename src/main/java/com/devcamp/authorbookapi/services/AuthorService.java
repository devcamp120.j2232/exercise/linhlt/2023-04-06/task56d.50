package com.devcamp.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.models.Author;

@Service
public class AuthorService {
    Author author1 = new Author("Victor Hugo", "victor@yahoo.com", 'm');
    Author author2 = new Author("Marie Joy", "marie@gmail.com", 'f');
    Author author3 = new Author("Linh Lai", "linh@gmail.com", 'f');
    Author author4 = new Author("Micheal Korrs", "micheal@yahoo.com", 'm');
    Author author5 = new Author("Angelina Jolly", "jolly@gmail.com", 'f');
    Author author6 = new Author("Phoebe Wang", "phoebe@hotmail.com", 'f');
    Author author7 = new Author("Peter Pan", "peter@yahoo.com", 'm');
    Author author8 = new Author("Becky Phan", "beckyphan@gmail.com", 'f');
    Author author9 = new Author("Emma Watson", "emma@gmail.com", 'f');
    Author author10 = new Author("Harry James", "james@yahoo.com", 'm');
    Author author11 = new Author("Honey Ham", "honey@gmail.com", 'f');
    Author author12 = new Author("Song Hye Ko", "shk@hotmail.com", 'f');
    public ArrayList<Author> getAllAuthors(){
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(author1);
        authorList.add(author2);
        authorList.add(author3);
        authorList.add(author4);
        authorList.add(author5);
        authorList.add(author6);
        authorList.add(author7);
        authorList.add(author8);
        authorList.add(author9);
        authorList.add(author10);
        authorList.add(author11);
        authorList.add(author12);
        return authorList;
    }
    
    public ArrayList<Author> getAuthorBook1(){
        ArrayList<Author> authorBook1 = new ArrayList<>();
        authorBook1.add(author1);
        authorBook1.add(author2);
        return authorBook1;
    }
    public ArrayList<Author> getAuthorBook2(){
        ArrayList<Author> authorBook2 = new ArrayList<>();
        authorBook2.add(author3);
        authorBook2.add(author4);
        return authorBook2;
    }
    public ArrayList<Author> getAuthorBook3(){
        ArrayList<Author> authorBook3 = new ArrayList<>();
        authorBook3.add(author5);
        authorBook3.add(author6);
        return authorBook3;
    }
    public ArrayList<Author> getAuthorBook4(){
        ArrayList<Author> authorBook4 = new ArrayList<>();
        authorBook4.add(author7);
        authorBook4.add(author8);
        return authorBook4;
    }
    public ArrayList<Author> getAuthorBook5(){
        ArrayList<Author> authorBook5 = new ArrayList<>();
        authorBook5.add(author9);
        authorBook5.add(author10);
        return authorBook5;
    }
    public ArrayList<Author> getAuthorBook6(){
        ArrayList<Author> authorBook6 = new ArrayList<>();
        authorBook6.add(author11);
        authorBook6.add(author12);
        return authorBook6;
    }
}
