package com.devcamp.authorbookapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.authorbookapi.models.Author;
import com.devcamp.authorbookapi.services.AuthorService;
@RestController
@RequestMapping("/")
@CrossOrigin
public class AuthorController {
    @Autowired
    AuthorService authorService;
    
    @GetMapping("/author-info")
    public Author getAuthorApi(@RequestParam String email){
        Author author = null;
        for (int i = 0; i < authorService.getAllAuthors().size() ; i++){
            if (authorService.getAllAuthors().get(i).getEmail().equals(email)){
                author = authorService.getAllAuthors().get(i);
            }
        }
        return author;
    }
    @GetMapping("/author-gender")
    public ArrayList<Author> authorsByGenderApi(@RequestParam char gender){
        ArrayList<Author> authors = new ArrayList<>();
        for (int i = 0 ; i < authorService.getAllAuthors().size() ; i++){
            if (authorService.getAllAuthors().get(i).getGender() == gender){
                authors.add(authorService.getAllAuthors().get(i));
            }
        }
        return authors;
    }
}
